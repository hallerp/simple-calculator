
// Simple Calculator
// Paul Haller

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool Divide(float num1, float num2, float& answer);
float Pow(float, int);

int main()
{
	char restart;
	do
	{
		float num1, num2, answer;
		char arithmetic;

		cout << "Enter the first positive number\n";

		while (cin >> num1 && num1 < 0)
		{
			cout << "Error: Please enter positive number\n";
		}

		cout << "Please select from the following: (+) (-) (*) (/) (^)\n";

		while (cin >> arithmetic && (arithmetic != '+' && arithmetic != '-' && arithmetic != '*' && arithmetic != '/' && arithmetic != '^'))
		{
			cout << "Error: Please enter a valid operator\n";
		}

		cout << "Enter the second positive number\n";

		while (cin >> num2 && num2 < 0)
		{
			cout << "Error: Please enter positive number\n";
		}


		switch (arithmetic)
		{
		case '+':
			answer = Add(num1, num2);
			break;
		case '-':
			answer = Subtract(num1, num2);
			break;
		case '*':
			answer = Multiply(num1, num2);
			break;
		case '/':
			while (!Divide(num1, num2, answer))
			{
				cout << "Second number cannot be 0!\n";
				cin >> num2;
			}
			break;
		case '^':
			answer = Pow(num1, num2);
			break;
		default:
			cout << "please select a valid operator";
			break;
		}

		cout << "Answer = " << answer;



		cout << "\nWould you like to perform another calculation? (select \"Y\" to restart or anything else to exit)\n";

		cin >> restart;

		if (restart != 'y')
		{
			cout << "Press any key to exit";
			(void)_getch();
			return 0;
		}

	} while (restart == 'y');


}

float Add(float num1, float num2)
{
	return num1 + num2;
}

float Subtract(float num1, float num2)
{
	return num1 - num2;
}

float Multiply(float num1, float num2)
{
	return num1 * num2;
}

bool Divide(float num1, float num2, float& answer)
{
	if (num2 == 0)
	{
		return false;
	}
	else
	{
		answer = num1 / num2;
		return true;
	}
}

float Pow(float num1, int num2)
{
	if (num2 != 0)
		return (num1 * Pow(num1, num2 - 1));
	else
		return 1;
}